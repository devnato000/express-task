import express from 'express';
import cors from 'cors';
import loggerMiddleware from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import groupsRouter from '../groups/group.router';
import courseRouter from '../courses/courses.router';
import lectorRouter from '../lectors/lectors.router';
import markRouter from '../marks/marks.router';
import bodyParser from 'body-parser';
import exceptionFilter from './middlewares/exceptions.filter';
import path from 'path';
import { AppDataSource } from '../configs/database/data-source';
import postsRouter from '../posts/posts.router';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const app = express();

app.use(bodyParser.json());
app.use(cors());

export const appDataSourceInit = async () => {
  return await AppDataSource.initialize();
};
app.use(loggerMiddleware);

const options = {
  definition: {
    openapi: '3.1.0',
    info: {
      title: 'University API ',
      version: '0.1.0',
      description: 'University API',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
      contact: {
        name: 'LogRocket',
        url: 'https://logrocket.com',
        email: 'info@email.com',
      },
    },
    servers: [
      {
        url: 'http://localhost:3000/api/v1',
      },
    ],
  },
  apis: ['./**/*.router.ts', './**/*.interface.ts'],
};

const specs = swaggerJsdoc(options);
app.use('/api/v1/docs', swaggerUi.serve, swaggerUi.setup(specs));

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/courses', courseRouter);
app.use('/api/v1/lectors', lectorRouter);
app.use('/api/v1/marks', markRouter);
app.use('/api/v1/posts', postsRouter);
app.use(exceptionFilter);

export default app;
