import {
  BaseEntity,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export abstract class CoreEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: string;

  @CreateDateColumn({ type: 'timestamp with time zone', name: 'create_at' })
  public createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp with time zone', name: 'update_at' })
  public updateAt: Date;
}
