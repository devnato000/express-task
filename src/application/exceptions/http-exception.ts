class HttpException extends Error {
  status: number;
  massage: string;

  constructor(status: number, message: string) {
    super(message);
    this.status = status;
    this.massage = message;
  }
}

export default HttpException;
