import Joi from 'joi';
import { Course } from './entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';

export const courseCreateSchema = Joi.object<Omit<Course, 'id'>>({
  name: Joi.string().required(),
  description: Joi.string().required(),
  hours: Joi.number().required(),
});

export const courseUpdateSchema = Joi.object<Partial<Course>>({
  name: Joi.string().optional(),
  description: Joi.string().optional(),
  hours: Joi.number().optional(),
});

export const courseAddLectorSchema = Joi.object<Pick<Course, 'lectors'>>({
  lectors: Joi.number().required(),
});

export const addLectorToCourseIdParamSchema = Joi.object<{
  courseId: string;
  lectorId: string;
}>({
  courseId: Joi.string().required(),
  lectorId: Joi.string().required(),
});
