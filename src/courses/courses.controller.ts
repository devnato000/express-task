import { Request, Response } from 'express';
import * as courseService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ICourseCreateRequest } from './types/course-create-request.interface';
import {
  IAddLectorToCourseRequest,
  ICourseUpdateRequest,
} from './types/course-update-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';

export const getAllCourses = async (request: Request, response: Response) => {
  const courses = await courseService.getAllCourses();
  response.json(courses);
};

export const getCoursesByLectorId = async (
  request: Request<{ lectorId: string }>,
  response: Response,
) => {
  const { lectorId } = request.params;
  const courses = await courseService.getCoursesByLectorId(lectorId);
  response.status(HttpStatuses.OK).json(courses);
};

export const getCoursesById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const courses = await courseService.getCourseById(id);
  response.status(HttpStatuses.OK).json(courses);
};

export const getCoursesWithMarks = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const courses = await courseService.getCoursesWithMarks(id);
  response.status(HttpStatuses.OK).json(courses);
};

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response,
) => {
  const course = await courseService.createCourse(request.body);
  response.status(HttpStatuses.OK).json(course);
};

export const updateCourseById = async (
  request: ValidatedRequest<ICourseUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await courseService.updateCourseById(id, request.body);

  if (course) {
    response.status(HttpStatuses.OK).json(course);
  } else {
    response
      .status(HttpStatuses.NOT_FOUND)
      .json({ error: 'Course is not found' });
  }
};

export const addLectorToCourse = async (
  request: ValidatedRequest<IAddLectorToCourseRequest>,
  response: Response,
) => {
  const { courseId, lectorId } = request.params;
  const course = await courseService.addLectorToCourse(courseId, lectorId);
  response.status(HttpStatuses.NO_CONTENT).json(course);
};

export const deleteCourseById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await courseService.deleteCourseById(id);

  if (course) {
    response.status(HttpStatuses.NO_CONTENT).json(course);
  } else {
    response
      .status(HttpStatuses.NOT_FOUND)
      .json({ error: 'Course is not found' });
  }
};
