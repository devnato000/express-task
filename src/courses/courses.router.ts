import express from 'express';
import * as coursesController from './courses.controller';
import controllerWrapper from '../application/utility/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  addLectorToCourseIdParamSchema,
  courseCreateSchema,
  courseUpdateSchema,
} from './course.schema';

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Course
 *   description: The students managing API
 * /courses:
 *   get:
 *     summary: Get all Courses
 *     tags: [Course]
 *
 *     responses:
 *       200:
 *         description: List of students.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Course'
 *       500:
 *         description: Some server error
 *
 */

router.get('/', controllerWrapper(coursesController.getAllCourses));

/**
 * @swagger
 * tags:
 *   name: Course
 *   description: The courses managing API
 * /courses/{id}:
 *   get:
 *     summary: Get Course by id
 *     tags: [Course]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: Course id
 *     responses:
 *       200:
 *         description: The Course by id.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/MarkCourse'
 *       404:
 *         description: The Course was not found
 *       500:
 *         description: Some server error
 *
 */

router.get('/:id', controllerWrapper(coursesController.getCoursesById));

/**
 * @swagger
 * tags:
 *   name: Course
 *   description: The courses managing API
 * /courses/{id}/marks:
 *   get:
 *     summary: Get Course by id with marks and students
 *     tags: [Course]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: Course id
 *     responses:
 *       200:
 *         description: The Course by id.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/CourseMarkWithStudents'
 *       404:
 *         description: The Course was not found
 *       500:
 *         description: Some server error
 *
 */

router.get(
  '/:id/marks',
  controllerWrapper(coursesController.getCoursesWithMarks),
);

/**
 * @swagger
 * tags:
 *   name: Course
 *   description: The courses managing API
 * /courses:
 *   post:
 *     summary: Create course
 *     tags: [Course]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/CourseCreate'
 *     responses:
 *       200:
 *         description: Return the student after creation .
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Course'
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 *
 */

router.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse),
);
router.patch(
  '/:id',
  validator.body(courseUpdateSchema),
  controllerWrapper(coursesController.updateCourseById),
);

/**
 * @swagger
 * tags:
 *   name: Course
 *   description: The courses managing API
 * /courses/{courseId}/lector/{lectorId}:
 *   patch:
 *     summary: Add lector to course
 *     tags: [Course]
 *     parameters:
 *       - in: path
 *         name: courseId
 *         schema:
 *           type: string
 *         required: true
 *         description: course id
 *       - in: path
 *         name: lectorId
 *         schema:
 *           type: string
 *         required: true
 *         description: lector id
 *     requestBody:
 *       required: false
 *     responses:
 *       204:
 *         description: Lector added to the course
 *       404:
 *         description: The Course or Lector was not found
 *       500:
 *         description: Internal Server Error
 */

router.patch(
  '/:courseId/lector/:lectorId',
  validator.params(addLectorToCourseIdParamSchema),
  controllerWrapper(coursesController.addLectorToCourse),
);

/**
 * @swagger
 * tags:
 *   name: Course
 *   description: The courses managing API
 * /courses/{id}:
 *  delete:
 *     summary: Remove course by id
 *     tags: [Course]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Course id
 *     responses:
 *       204:
 *         description: The Course was deleted
 *       404:
 *         description: The Course was not found
 */

router.delete('/:id', controllerWrapper(coursesController.deleteCourseById));

export default router;
