import { Course } from './entities/course.entity';
import { AppDataSource } from '../configs/database/data-source';
import HttpException from '../application/exceptions/http-exception';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { DeleteResult, UpdateResult } from 'typeorm';
import { LectorCourse } from '../lectors/entities/lector_course.entity';
import { getLectorById } from '../lectors/lectors.service';

const courseRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async () => {
  return courseRepository.find({ relations: ['lectors', 'marks'] });
};

export const getCourseById = async (courseId: string) => {
  const course = await courseRepository.findOne({
    where: {
      id: courseId,
    },
  });

  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }
  return course;
};

export async function getCoursesWithMarks(id: string) {
  const course = await courseRepository.find({
    where: {
      id: id,
    },
    relations: {
      marks: {
        student: true,
      },
    },
  });

  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }
  return course;
}

export const getCoursesByLectorId = async (
  lectorId: string,
): Promise<Course[]> => {
  const lectorCourseRepository = AppDataSource.getRepository(LectorCourse);

  const lectorCourses = await lectorCourseRepository
    .createQueryBuilder('lector')
    .innerJoinAndSelect('lector.courses', 'courses')
    .where('lector.lectorId = :lectorId', { lectorId })
    .getMany();
  const courses = lectorCourses.flatMap((lc) => lc.courses);

  return courses;
};

export const createCourse = async (
  courseCreateSchema: Omit<Course, 'id'>,
): Promise<Course> => {
  const course = await courseRepository.findOne({
    where: {
      name: courseCreateSchema.name,
    },
  });
  if (course) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Course with this name already exists',
    );
  }
  return courseRepository.save(courseCreateSchema);
};

export const updateCourseById = async (
  id: string,
  courseUpdateSchema: Partial<Course>,
): Promise<UpdateResult> => {
  const result = await courseRepository.update(id, courseUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course with this id is not found',
    );
  }
  return result;
};

export const addLectorToCourse = async (
  courseId: string,
  lectorId: string,
): Promise<void> => {
  const lectorCourseRepository = AppDataSource.getRepository(LectorCourse);
  const course = await getCourseById(courseId);
  const lector = await getLectorById(lectorId);
  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }
  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }
  await lectorCourseRepository
    .createQueryBuilder()
    .insert()
    .into(LectorCourse)
    .values({
      lectorId: lectorId,
      courseId: courseId,
    })
    .execute();
};

export const deleteCourseById = async (id: string): Promise<DeleteResult> => {
  const result = await courseRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course with this id is not found',
    );
  }
  return result;
};
