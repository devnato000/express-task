import {
  Entity,
  Column,
  ManyToMany,
  OneToMany,
  JoinColumn,
  JoinTable,
  ManyToOne,
} from 'typeorm';
import { Lector } from '../../lectors/entities/lector.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { CoreEntity } from '../../application/entities/core.entity';
import { Student } from '../../students/entities/student.entity';

@Entity({ name: 'course' })
export class Course extends CoreEntity {
  @Column({ type: 'varchar', nullable: false })
  name: string;

  @Column({ type: 'varchar', nullable: false })
  description: string;

  @Column({ type: 'varchar', nullable: false })
  hours: number;

  @ManyToMany(() => Lector, (lector) => lector.courses)
  lectors?: Lector[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];
}
