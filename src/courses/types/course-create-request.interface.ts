import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Course } from '../entities/course.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseCreate:
 *       type: object
 *       required:
 *         - name
 *         - description
 *         - hors
 *       properties:
 *         name:
 *           type: string
 *           description: student name
 *         description:
 *           type: string
 *           description: student surname
 *         hors:
 *           type: number
 *           description: student email
 *       example:
 *         name: course1
 *         description: description for course1
 *         hours: 10
 */

export interface ICourseCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Course, 'id'>;
}
