/**
 * @swagger
 * components:
 *   schemas:
 *     Course:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - description
 *         - hours
 *         - lectors
 *         - marks
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: course name
 *         description:
 *           type: string
 *           description: mark course
 *         hours:
 *           type: string
 *           description: course hours
 *         lectors:
 *           type: ['array', 'null']
 *           description: mark course
 *           items:
 *              $ref: '#/components/schemas/LectorForCourse'
 *         marks:
 *           type: ['array', 'null']
 *           description: course hours
 *           items:
 *              $ref: '#/components/schemas/MarkForCourse'
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: course1
 *         description: desc for course 2
 *         hours: 20
 *         lectors: []
 *         marks: []
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseMarkWithStudents:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - description
 *         - hours
 *         - marks
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: course name
 *         description:
 *           type: string
 *           description: mark course
 *         hours:
 *           type: string
 *           description: course hours
 *         marks:
 *           type: ['array', 'null']
 *           description: course hours
 *           items:
 *              $ref: '#/components/schemas/Mark'
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: course1
 *         description: desc for course 2
 *         hours: 20
 *         marks: []
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     MarkCourse:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - description
 *         - hours
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: course name
 *         description:
 *           type: string
 *           description: mark course
 *         hours:
 *           type: string
 *           description: course hours
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: course1
 *         description: desc for course 2
 *         hours: 20
 */
