import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Course } from '../entities/course.entity';

export interface ICourseUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Course>;
}
export interface IAddLectorToCourseRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<Course, 'lectors'>;
}

//////////////////////////////////////////
// @Controller('/student')
// export class StudentController {
//   constructor(
//       private readonly studentService: StudentService,
//   ) {}
//
//   @Post()
//   async createStudentCourse(@Body() createStudentCourse: {studentId: number, courseId: number}) {
//     await this.studentService.createStudentCorse(createStudentCourse);
//   }
//
//   @Injectable()
//   export class StudentService {
//   constructor(
//       @InjectRepository(Student)
//       private readonly studentRepository: Repository<Student>,
//       @InjectRepository(StudentCourse)
//       private readonly studentCourseRepository: Repository<StudentCourse>
//   ) {}
//
//
//   async createStudentCourse(createStudentCourse: {studentId: number, courseId: number}): Promise<void> {
//     const student = await this.studentReposioty.findOne({where: {id: createStudentCourse.studentId}});
//     if(!student){
//       throw new NotFoundException()
//     }
//     /* You can check if course with given ID exists aswell here in the same way with CourseRepository */
//
//     await this.studentCourseRepository.save(createStudentCourse)
//
//
//   }
