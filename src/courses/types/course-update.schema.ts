import Joi from 'joi';
import { Lector } from '../../lectors/entities/lector.entity';

export const courseUpdateGroupSchema = Joi.object<Pick<Lector, 'id'>>({
  id: Joi.number().required(),
});
