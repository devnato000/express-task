import { Request, Response } from 'express';
import * as groupsService from './group.service';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupUpdateRequest } from './types/group-update-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';

export const getAllGroupsWithStudents = async (
  request: Request,
  response: Response,
) => {
  const groups = await groupsService.getAllGroupsWithStudents();
  response.status(HttpStatuses.OK).json(groups);
};

export const getGroupWithStudents = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const groupWithStudents = await groupsService.getStudentByGroupId(id);
  response.status(HttpStatuses.OK).json(groupWithStudents);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = await groupsService.createGroup(request.body);
  response.status(HttpStatuses.OK).json(group);
};

export const updateGroupById = (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.updateGroupById(id, request.body);
  if (group) {
    response.status(HttpStatuses.NO_CONTENT).json(group);
  } else {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Group not found' });
  }
};

export const deleteGroupById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.deleteGroupById(id);
  if (group) {
    response.status(HttpStatuses.NO_CONTENT).json(group);
  } else {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Group not found' });
  }
};
