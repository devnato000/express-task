import HttpException from '../application/exceptions/http-exception';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { AppDataSource } from '../configs/database/data-source';
import { Group } from './entities/group.entity';
import httpStatusesEnum from '../application/enums/http-statuses.enum';

const groupsRepository = AppDataSource.getRepository(Group);

export const getAllGroupsWithStudents = async (): Promise<Group[]> => {
  return groupsRepository.find({ relations: ['student'] });
};

export const getStudentByGroupId = async (id: string): Promise<Group> => {
  const group = await groupsRepository
    .createQueryBuilder('groups')
    .leftJoinAndSelect('groups.student', 'student')
    .where('groups.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }
  return group;
};
export const createGroup = async (
  groupCreateSchema: Omit<Group, 'id'>,
): Promise<Group> => {
  const group = await groupsRepository.findOne({
    where: {
      name: groupCreateSchema.name,
    },
  });
  if (group) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Group with this name already exists',
    );
  }

  return groupsRepository.save(groupCreateSchema);
};

export const updateGroupById = async (
  id: string,
  groupUpdateSchema: Partial<Group>,
) => {
  const result = await groupsRepository.update(id, groupUpdateSchema);
  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }
};

export const deleteGroupById = async (id: string) => {
  const group = getStudentByGroupId(id);

  if (!group) {
    throw new HttpException(httpStatusesEnum.NOT_FOUND, 'Group not found');
  }

  await groupsRepository.delete(id);
};
