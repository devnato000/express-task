import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Group } from '../entities/group.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupCreate:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         name:
 *           type: string
 *           description: group name
 *       example:
 *         name: math group
 */

export interface IGroupCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Group, 'id'>;
}
