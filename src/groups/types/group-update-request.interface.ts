import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Group } from '../entities/group.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupUpdate:
 *       type: object
 *       properties:
 *        name:
 *          type: string
 *       example:
 *         name: math group
 */

export interface IGroupUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Group>;
}
