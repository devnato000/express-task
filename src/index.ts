import 'dotenv/config';
import app, { appDataSourceInit } from './application/app';

const port = process.env.SERVER_PORT;

const startServer = async () => {
  app.listen(port, () => {
    appDataSourceInit()
      .then(() => {
        console.log('Typeorm connected to database');
      })
      .catch((error) => {
        console.log('Error: ', error);
      });
    console.log(`Server started at port ${port}`);
  });
};

startServer();
