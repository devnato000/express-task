import { Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Lector } from './lector.entity';
import { Course } from '../../courses/entities/course.entity';
import { CoreEntity } from '../../application/entities/core.entity';

@Entity({ name: 'lector_course' })
export class LectorCourse extends CoreEntity {
  @PrimaryColumn({ name: 'lector_id' })
  lectorId: string;

  @PrimaryColumn({ name: 'course_id' })
  courseId: string;

  @ManyToOne(() => Lector, (lector) => lector.courses, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'lector_id', referencedColumnName: 'id' }])
  lectors: Lector[];

  @ManyToOne(() => Course, (course) => course.lectors, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'course_id', referencedColumnName: 'id' }])
  courses: Course[];
}
