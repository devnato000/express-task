import { Request, Response } from 'express';
import * as lectorService from './lectors.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorCreateRequest } from './types/lector-create-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { ILectorUpdateRequest } from './types/lector-update-request.interface';

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response,
) => {
  const lector = await lectorService.createLector(request.body);
  response.json(lector);
};

export const getLectorWithCoursesById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorService.getLectorWithCoursesById(id);
  if (!lector) {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Lector not found' });
  }
  response.json(lector);
};

export const getAllLectors = async (request: Request, res: Response) => {
  const lectors = await lectorService.getAllLectors();
  res.json(lectors);
};

export const updateLectorById = async (
  request: ValidatedRequest<ILectorUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorService.updateLectorById(id, request.body);

  if (lector) {
    response.status(HttpStatuses.NO_CONTENT).json(lector);
  } else {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Lector not found' });
  }
};

export const deleteLectorById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorService.deleteLectorById(id);

  if (lector) {
    response.status(HttpStatuses.NO_CONTENT).json(lector);
  } else {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Lector not found' });
  }
};
