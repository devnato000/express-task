import express from 'express';
import * as lectorsController from './lectors.controller';
import controllerWrapper from '../application/utility/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { lectorUpdateSchema } from './lectors.schema';

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors:
 *   get:
 *     summary: Get all lectors
 *     tags: [Lectors]
 *
 *     responses:
 *       200:
 *         description: List of lectors.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Lector'
 *       500:
 *         description: Some server error
 *
 */

router.get('/', controllerWrapper(lectorsController.getAllLectors));

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *   get:
 *     summary: Get lector by id
 *     tags: [Lectors]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: lector id
 *     responses:
 *       200:
 *         description: The lector by id.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/LectorById'
 *       404:
 *         description: The Lector was not found
 *       500:
 *         description: Some server error
 *
 */

router.get(
  '/:id',
  controllerWrapper(lectorsController.getLectorWithCoursesById),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors:
 *   post:
 *     summary: Create lector
 *     tags: [Lectors]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/LectorCreate'
 *     responses:
 *       200:
 *         description: Return the lector after creation .
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Lector'
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 *
 */

router.post('/', controllerWrapper(lectorsController.createLector));

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *   patch:
 *     summary: Update lectors by id
 *     tags: [Lectors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Lector id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/LectorUpdate'
 *     responses:
 *       204:
 *         description: The Lector was updated
 *       404:
 *         description: The Lector was not found
 */

router.patch(
  '/:id',
  validator.body(lectorUpdateSchema),
  controllerWrapper(lectorsController.updateLectorById),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *  delete:
 *     summary: Remove lector by id
 *     tags: [Lectors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Lector id
 *     responses:
 *       204:
 *         description: The Lector was deleted
 *       404:
 *         description: The Lector was not found
 */

router.delete('/:id', controllerWrapper(lectorsController.deleteLectorById));

export default router;
