import Joi from 'joi';
import { Lector } from './entities/lector.entity';

export const lectorUpdateSchema = Joi.object<Partial<Lector>>({
  name: Joi.string().optional(),
  email: Joi.string().optional(),
  password: Joi.number().optional(),
});
