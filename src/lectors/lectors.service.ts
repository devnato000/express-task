import { Lector } from './entities/lector.entity';
import { AppDataSource } from '../configs/database/data-source';
import HttpException from '../application/exceptions/http-exception';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { DeleteResult, UpdateResult } from 'typeorm';

const lectorRepository = AppDataSource.getRepository(Lector);

export const getAllLectors = async (): Promise<Lector[]> => {
  return lectorRepository.find({ relations: ['courses', 'marks'] });
};

export const getLectorById = async (id: string): Promise<Lector> => {
  const lector = await lectorRepository
    .createQueryBuilder('lector')
    .select([
      'lector.id as id',
      'lector.name as name',
      'lector.email as email',
      'lector.password as password',
    ])
    .where('lector.id = :id', { id })
    .getRawOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'lector not found');
  }
  return lector;
};

export const getLectorWithCoursesById = async (
  id: string,
): Promise<Lector[]> => {
  return lectorRepository.find({
    where: { id },
    relations: {
      courses: true,
    },
  });
};

export const createLector = async (
  lectorCreateSchema: Omit<Lector, 'id'>,
): Promise<Lector> => {
  const lector = await lectorRepository.findOne({
    where: {
      email: lectorCreateSchema.email,
    },
  });
  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this email already exists',
    );
  }
  return lectorRepository.save(lectorCreateSchema);
};

export const updateLectorById = async (
  id: string,
  lectorUpdateSchema: Partial<Lector>,
): Promise<UpdateResult> => {
  const result = await lectorRepository.update(id, lectorUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Lector with this id is not found',
    );
  }
  return result;
};

export const deleteLectorById = async (id: string): Promise<DeleteResult> => {
  const result = await lectorRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Lector with this id is not found',
    );
  }
  return result;
};
