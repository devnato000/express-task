import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Lector } from '../entities/lector.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorCreate:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - password
 *       properties:
 *         name:
 *           type: string
 *           description: student name
 *         email:
 *           type: string
 *           description: student surname
 *         password:
 *           type: string
 *           description: student email
 *       example:
 *         name: Ivan
 *         email: Ivan@mail
 *         password: pass
 */

export interface ILectorCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Lector, 'id'>;
}
