/**
 * @swagger
 * components:
 *   schemas:
 *     Lector:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - email
 *         - password
 *         - courses
 *         - marks
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: lector name
 *         email:
 *           type: string
 *           description: lector email
 *         password:
 *           type: string
 *           description: lector password
 *         courses:
 *           type: ['array', 'null']
 *           description: lector courses
 *           items:
 *              $ref: '#/components/schemas/Course'
 *         marks:
 *           type: ['array', 'null']
 *           description: lector marks
 *           items:
 *              $ref: '#/components/schemas/Marks'
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Ivan
 *         email: Ivan@mail
 *         password: pass
 *         age: 20
 *         courses: []
 *         marks: []
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorById:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - email
 *         - password
 *         - courses
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: lector name
 *         email:
 *           type: string
 *           description: lector email
 *         password:
 *           type: string
 *           description: lector password
 *         courses:
 *           type: ['array', 'null']
 *           description: lector courses
 *           items:
 *              $ref: '#/components/schemas/Course'
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Ivan
 *         email: Ivan@mail
 *         password: pass
 *         age: 20
 *         courses: []
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorForCourse:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - email
 *         - password
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: lector name
 *         email:
 *           type: string
 *           description: lector email
 *         password:
 *           type: string
 *           description: lector password
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Ivan
 *         email: Ivan@mail
 *         password: pass
 */
