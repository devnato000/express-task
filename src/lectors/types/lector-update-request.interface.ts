import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Lector } from '../entities/lector.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorUpdate:
 *       type: object
 *       properties:
 *        name:
 *          type: string
 *          description: lector name
 *        email:
 *          type: string
 *          description: lector surname
 *        password:
 *          type: string
 *          description: lector email
 *       example:
 *         name: Ivan
 *         email: Ivan@mail
 *         password: pass
 */

export interface ILectorUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Lector>;
}
