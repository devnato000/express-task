import { Entity, Column, ManyToOne } from 'typeorm';
import { Student } from '../../students/entities/student.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Course } from '../../courses/entities/course.entity';
import { CoreEntity } from '../../application/entities/core.entity';

@Entity()
export class Mark extends CoreEntity {
  @Column()
  mark: string;

  @ManyToOne(() => Student, (student) => student.marks)
  student: Student;

  @ManyToOne(() => Lector, (lector) => lector.marks)
  lector: Lector;

  @ManyToOne(() => Course, (course) => course.marks)
  course: Course;
}
