import { Request, Response } from 'express';
import * as markService from './marks.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IAddMarkRequest } from './types/marks-update-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';

export const createMark = async (
  request: ValidatedRequest<IAddMarkRequest>,
  response: Response,
) => {
  const { mark, course, student, lector } = request.body;
  const markData = { mark, course, student, lector };
  const markEntity = await markService.createMark(markData);
  response.status(HttpStatuses.NO_CONTENT).json(markEntity);
};
export const deleteMark = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const markEntity = await markService.deleteMark(id);
  response.status(HttpStatuses.NO_CONTENT).json(markEntity);
};
