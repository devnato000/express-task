import express from 'express';
import * as marksController from './marks.controller';
import controllerWrapper from '../application/utility/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { marksUpdateGroupSchema } from './marks.schema';
import { idParamSchema } from '../application/schemas/id-param.schema';

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks:
 *   post:
 *     summary: Create mark
 *     tags: [Marks]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/MarkCreate'
 *     responses:
 *       204:
 *         description: Mark created .
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 *
 */

router.post(
  '/',
  validator.body(marksUpdateGroupSchema),
  controllerWrapper(marksController.createMark),
);

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks/{id}:
 *  delete:
 *     summary: Remove marks by id
 *     tags: [Marks]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: marks id
 *     responses:
 *       204:
 *         description: The Mark was deleted
 *       404:
 *         description: The Mark was not found
 */

router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(marksController.deleteMark),
);

export default router;
