import Joi from 'joi';
import { Mark } from './entities/mark.entity';

export const marksUpdateGroupSchema = Joi.object<Partial<Mark>>({
  mark: Joi.number().required(),
  student: Joi.number().required(),
  lector: Joi.number().required(),
  course: Joi.number().required(),
});
