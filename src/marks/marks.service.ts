import { Mark } from './entities/mark.entity';
import { Student } from '../students/entities/student.entity';
import { Course } from '../courses/entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { AppDataSource } from '../configs/database/data-source';
import HttpException from '../application/exceptions/http-exception';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { DeleteResult } from 'typeorm';
import httpStatusesEnum from '../application/enums/http-statuses.enum';

const markRepository = AppDataSource.getRepository(Mark);

export const createMark = async (
  markCreateSchema: Partial<Mark>,
): Promise<void> => {
  const { mark, course, student, lector } = markCreateSchema;

  const isCourse = await AppDataSource.getRepository(Course).find({
    where: { id: `${course}` },
  });
  const isStudent = await AppDataSource.getRepository(Student).find({
    where: { id: `${student}` },
  });
  const isLector = await AppDataSource.getRepository(Lector).find({
    where: { id: `${lector}` },
  });

  if (!isCourse) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }
  if (!isStudent) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'student not found');
  }
  if (!isLector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'lector not found');
  }
  await markRepository
    .createQueryBuilder()
    .insert()
    .into(Mark)
    .values({
      mark: mark,
      student: student,
      lector: lector,
      course: course,
    })
    .execute();
};

export const deleteMark = async (id: string): Promise<DeleteResult> => {
  const mark = await markRepository.delete(id);

  if (!mark.affected) {
    throw new HttpException(httpStatusesEnum.NOT_FOUND, 'Mark not found');
  }

  return mark;
};
