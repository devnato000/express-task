/**
 * @swagger
 * components:
 *   schemas:
 *     MarkCreate:
 *       type: object
 *       required:
 *         - mark
 *         - student
 *         - lector
 *         - course
 *       properties:
 *         mark:
 *           type: string
 *           description: student name
 *         student:
 *           type: string
 *           description: student surname
 *         lector:
 *           type: string
 *           description: student email
 *         course:
 *           type: number
 *           description: student age
 *       example:
 *         mark: 10
 *         student: 1
 *         lector: 1
 *         course: 1
 */
