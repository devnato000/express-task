/**
 * @swagger
 * components:
 *   schemas:
 *     Marks:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - mark
 *         - course
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         mark:
 *           type: string
 *           description: mark
 *         course:
 *           type: string
 *           description: mark course
 *           items:
 *              $ref: '#/components/schemas/MarkCourse'
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         mark: 5
 *         course: []
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     Mark:
 *       type: object
 *       required:
 *         - id
 *         - mark
 *         - student
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id
 *         mark:
 *           type: string
 *           description: mark
 *         student:
 *           type: string
 *           description: mark student
 *           items:
 *              $ref: '#/components/schemas/Student'
 *       example:
 *         id: 1
 *         mark: 5
 *         student: []
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     MarkForCourse:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - mark
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         mark:
 *           type: string
 *           description: mark
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         mark: 5
 */
