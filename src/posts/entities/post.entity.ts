import { CoreEntity } from '../../application/entities/core.entity';
import { Column, Entity } from 'typeorm';

@Entity({ name: 'posts' })
export class Post extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  title: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  content: string;
}
