import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as postsService from './post.service';
import { IPostCreateRequest } from './types/post-create-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { IPostUpdateRequest } from './types/post-update-request.interface';

export const getAllPosts = async (request: Request, response: Response) => {
  const students = await postsService.getAllPosts();
  response.json(students);
};

export const getPostById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const post = await postsService.getPostById(id);
  console.log(post);
  if (!post) {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Post not found' });
  }
  response.status(HttpStatuses.OK).json(post);
};

export const createPost = async (
  request: ValidatedRequest<IPostCreateRequest>,
  response: Response,
) => {
  const post = await postsService.createPost(request.body);
  response.status(HttpStatuses.CREATED).json(post);
};

export const updatePost = async (
  request: ValidatedRequest<IPostUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;

  const post = await postsService.updatePost(id, request.body);

  response.status(HttpStatuses.NO_CONTENT).json(post);
};

export const deletePost = async (request: Request, response: Response) => {
  const { id } = request.params;
  const post = await postsService.deletePost(id);
  response.status(HttpStatuses.NO_CONTENT).json(post);
};
