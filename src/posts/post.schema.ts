import Joi from 'joi';
import { IPost } from './types/post.interface';

export const postCreateSchema = Joi.object<Omit<IPost, 'id'>>({
  title: Joi.string().required(),
  content: Joi.string().required(),
});

export const postUpdateSchema = Joi.object<Omit<IPost, 'id'>>({
  title: Joi.string(),
  content: Joi.string(),
});
