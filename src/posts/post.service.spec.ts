import { createPost, deletePost } from './post.service';
import { AppDataSource } from '../configs/database/data-source';
import { Post } from './entities/post.entity';
import HttpException from '../application/exceptions/http-exception';
import httpStatusesEnum from '../application/enums/http-statuses.enum';

const postsRepository = AppDataSource.getRepository(Post);

const mockedHttpException = HttpException as jest.MockedClass<
  typeof HttpException
>;
describe('post.service', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  describe('#createPost', () => {
    it('calls postsRepository.save with correct params', async () => {
      const spy = jest
        .spyOn(postsRepository, 'save')
        .mockImplementation((() => Promise.resolve()) as any);

      const title = 'example';
      const content = 'content';

      await createPost({ title, content });

      expect(spy.mock.calls).toEqual([[{ title, content }]]);
    });

    it('returns result of postsRepository.save', async () => {
      const result = {};

      jest
        .spyOn(postsRepository, 'save')
        .mockImplementation((() => Promise.resolve({})) as any);

      const title = 'example';
      const content = 'content';

      const actualResult = await createPost({ title, content });

      expect(actualResult).toEqual(result);
    });

    it('spy leak test', async () => {
      const spy = jest
        .spyOn(postsRepository, 'save')
        .mockImplementation((() => console.log('Spy from first test')) as any);

      await createPost({} as any);

      spy.mockRestore();
    });
  });

  describe('#deletePost', () => {
    it('calls postsRepository.delete with correct ID', async () => {
      const postId = '1';
      const spy = jest
        .spyOn(postsRepository, 'delete')
        .mockResolvedValue({ raw: {}, affected: 1 });

      await deletePost(postId);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(postId);
    });

    it('returns result of postsRepository.delete', async () => {
      const postId = 'exampleId';
      const result = { raw: {}, affected: 1 };
      jest.spyOn(postsRepository, 'delete').mockResolvedValue(result);

      const actualResult = await deletePost(postId);

      expect(actualResult).toEqual(result);
    });

    it('throws HttpException when the post does not exist', async () => {
      const postId = 'nonExistingPostId';
      jest
        .spyOn(postsRepository, 'delete')
        .mockResolvedValue({ raw: {}, affected: 0 });

      await expect(deletePost(postId)).rejects.toThrowError(
        new mockedHttpException(httpStatusesEnum.NOT_FOUND, 'post not found'),
      );

      expect(postsRepository.delete).toHaveBeenCalledTimes(1);
      expect(postsRepository.delete).toHaveBeenCalledWith(postId);
    });
  });
});
