import { AppDataSource } from '../configs/database/data-source';
import { IPost } from './types/post.interface';
import { Post } from './entities/post.entity';
import HttpException from '../application/exceptions/http-exception';
import httpStatusesEnum from '../application/enums/http-statuses.enum';
import HttpStatuses from '../application/enums/http-statuses.enum';

const postsRepository = AppDataSource.getRepository(Post);

export const getAllPosts = async (): Promise<Post[]> => {
  return postsRepository.find({
    order: {
      createdAt: 'desc',
    },
  });
};

export const getPostById = async (id: string): Promise<IPost | null> => {
  return postsRepository.findOne({ where: { id } });
};

export const createPost = async (
  postCreateSchema: Omit<IPost, 'id'>,
): Promise<Post> => {
  return postsRepository.save({
    title: postCreateSchema.title,
    content: postCreateSchema.content,
  });
};

export const updatePost = async (
  id: string,
  postUpdateSchema: Partial<IPost>,
) => {
  const post = await postsRepository.update(id, postUpdateSchema);
  if (!post.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Post not found');
  }
  return post;
};

export const deletePost = async (id: string) => {
  const post = await postsRepository.delete(id);
  if (!post.affected) {
    throw new HttpException(httpStatusesEnum.NOT_FOUND, 'post not found');
  }
  return post;
};
