import express from 'express';
import validator from '../application/middlewares/validation.middleware';
import { postCreateSchema, postUpdateSchema } from './post.schema';
import controllerWrapper from '../application/utility/controller-wrapper';
import * as postsController from './post.controller';
import { idParamSchema } from '../application/schemas/id-param.schema';

const router = express.Router();

router.get('/', controllerWrapper(postsController.getAllPosts));
router.get('/:id', controllerWrapper(postsController.getPostById));
router.post(
  '/',
  validator.body(postCreateSchema),
  controllerWrapper(postsController.createPost),
);
router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(postUpdateSchema),
  controllerWrapper(postsController.updatePost),
);
router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(postsController.deletePost),
);

export default router;
