import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Column({ type: 'varchar', nullable: false })
  name: string;

  @Column({ type: 'varchar', nullable: false })
  surname: string;

  @Column({ type: 'varchar', nullable: true })
  email: string;

  @Column({ type: 'numeric', nullable: true })
  age: number;

  @Column()
  imagePath: string;

  @ManyToOne(() => Group, (group) => group.student, {
    nullable: true,
    eager: false,
  })
  group: Group;

  @OneToMany(() => Mark, (mark) => mark.student, {
    nullable: false,
  })
  marks: Mark[];
}
