import { Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import {
  IAddGroupToStudentRequest,
  IStudentUpdateRequest,
} from './types/student-update-request.interface';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import HttpStatusesEnum from '../application/enums/http-statuses.enum';
import HttpStatuses from '../application/enums/http-statuses.enum';

export const getAllStudents = async (request: Request, response: Response) => {
  const { name } = request.query;
  const students = await studentsService.getAllStudents(name as string);
  response.status(HttpStatusesEnum.OK).json(students);
};

export const getStudentById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentById(id);
  response.json(student);
};

export const getStudentByIdWithMarks = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentByIdWithMarks(id);
  response.json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = await studentsService.createStudent(request.body);
  response.json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentById(id, request.body);
  response.status(HttpStatuses.NO_CONTENT).json(student);
};

export const addGroupToStudent = async (
  request: ValidatedRequest<IAddGroupToStudentRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.addGroupToStudent(id, request.body);
  response.status(HttpStatuses.NO_CONTENT).json(student);
};

export const deleteStudentById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);
  response.status(HttpStatuses.NO_CONTENT).json(student);
};
