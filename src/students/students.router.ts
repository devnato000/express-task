import { Router } from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../application/utility/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  studentCreateSchema,
  studentUpdateGroupSchema,
  studentUpdateSchema,
} from './students.schema';
import { idParamSchema } from '../application/schemas/id-param.schema';

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students:
 *   get:
 *     summary: Get all students
 *     tags: [Students]
 *
 *     responses:
 *       200:
 *         description: List of students.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Student'
 *       500:
 *         description: Some server error
 *
 */

router.get('/', controllerWrapper(studentsController.getAllStudents));

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   get:
 *     summary: Get student by id
 *     tags: [Students]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: Student id
 *     responses:
 *       200:
 *         description: The student by id.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/StudentByIdWithGroups'
 *       404:
 *         description: The Student was not found
 *       500:
 *         description: Some server error
 *
 */

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentById),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}/marks:
 *   get:
 *     summary: Get student by id
 *     tags: [Students]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: Student id
 *     responses:
 *       200:
 *         description: The student by id with marks.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/StudentsWithMarks'
 *       404:
 *         description: The student was not found
 *       500:
 *         description: Some server error
 */

router.get(
  '/:id/marks',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentByIdWithMarks),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students:
 *   post:
 *     summary: Create student
 *     tags: [Students]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/StudentCreate'
 *     responses:
 *       200:
 *         description: Return the student after creation .
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Student'
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 *
 */

router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   patch:
 *     summary: Update student by id
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/StudentUpdate'
 *     responses:
 *       204:
 *         description: The Student was updated
 *       404:
 *         description: The Student was not found
 */

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}/group:
 *   patch:
 *     summary: Update student by id
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/StudentUpdateGroup'
 *     responses:
 *       204:
 *         description: The Student was updated
 *       404:
 *         description: The Student was not found
 */

router.patch(
  '/:id/group',
  validator.params(idParamSchema),
  validator.body(studentUpdateGroupSchema),
  controllerWrapper(studentsController.addGroupToStudent),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *  delete:
 *     summary: Remove student by id
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     responses:
 *       204:
 *         description: The Student was deleted
 *       404:
 *         description: The Student was not found
 */
router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.deleteStudentById),
);

export default router;
