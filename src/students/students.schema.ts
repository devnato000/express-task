import Joi from 'joi';
import { Student } from './entities/student.entity';

export const studentCreateSchema = Joi.object<Omit<Student, 'id'>>({
  name: Joi.string().required(),
  surname: Joi.string().required(),
  email: Joi.string().required(),
  imagePath: Joi.string(),
  age: Joi.number().required(),
});

export const studentUpdateSchema = Joi.object<Partial<Student>>({
  name: Joi.string().optional(),
  surname: Joi.string().optional(),
  email: Joi.string().optional(),
  age: Joi.number().optional(),
});

export const studentUpdateGroupSchema = Joi.object<Pick<Student, 'group'>>({
  group: Joi.number().required(),
});
