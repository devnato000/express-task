import HttpException from '../application/exceptions/http-exception';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { DeleteResult, FindManyOptions, Like, UpdateResult } from 'typeorm';
import httpStatusesEnum from '../application/enums/http-statuses.enum';

const studentRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (name?: string): Promise<Student[]> => {
  const options: FindManyOptions<Student> = {};

  if (name) {
    options.where = { name: Like(`%${name}%`) };
  }
  const student = await studentRepository.find(options);

  if (!name) {
    return student;
  }

  return student.filter((student) =>
    student.name.toLowerCase().includes(name.toLowerCase()),
  );
};

export const getStudentById = async (id: string): Promise<Student[]> => {
  const student = await studentRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  return student;
};

export const getStudentByIdWithMarks = async (
  id: string,
): Promise<Student[]> => {
  const student = await studentRepository.find({
    where: { id },
    relations: {
      marks: {
        course: true,
      },
    },
  });

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  return student;
};

export const createStudent = async (
  createStudentSchema: Omit<Student, 'id'>,
): Promise<Student> => {
  const student = await studentRepository.findOne({
    where: {
      email: createStudentSchema.email,
    },
  });
  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exists',
    );
  }

  return studentRepository.save(createStudentSchema);
};

export const updateStudentById = async (
  id: string,
  studentUpdateSchema: Partial<Student>,
) => {
  const result = await studentRepository.update(id, studentUpdateSchema);
  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
};

export const deleteStudentById = async (id: string) => {
  const result = await studentRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(httpStatusesEnum.NOT_FOUND, 'Student not found');
  }
};

export const addGroupToStudent = async (
  id: string,
  studentUpdateSchema: Pick<Student, 'group'>,
) => {
  const student = await getStudentById(id);
  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  await studentRepository.update(id, studentUpdateSchema);
};
