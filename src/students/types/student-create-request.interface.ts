import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Student } from '../entities/student.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentCreate:
 *       type: object
 *       required:
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - imagePath
 *       properties:
 *         name:
 *           type: string
 *           description: student name
 *         surname:
 *           type: string
 *           description: student surname
 *         email:
 *           type: string
 *           description: student email
 *         age:
 *           type: number
 *           description: student age
 *         imagePath:
 *           type: string
 *           description: student image
 *       example:
 *         name: Ivan
 *         surname: Ivanov
 *         email: Ivan@mail
 *         age: 20
 *         imagePath: path
 */

export interface IStudentCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Student, 'id'>;
}
