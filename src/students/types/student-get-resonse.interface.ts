import { IStudent } from './student.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     Student:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - imagePath
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: student name
 *         surname:
 *           type: string
 *           description: student surname
 *         email:
 *           type: string
 *           description: student email
 *         age:
 *           type: number
 *           description: student age
 *         imagePath:
 *           type: string
 *           description: student image path
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Ivan
 *         surname: Ivanov
 *         email: Ivan@mail
 *         age: 20
 *         imagePath: path
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentByIdWithGroups:
 *       type: object
 *       required:
 *         - id
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - groupName
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         name:
 *           type: string
 *           description: student name
 *         surname:
 *           type: string
 *           description: student surname
 *         email:
 *           type: string
 *           description: student email
 *         age:
 *           type: number
 *           description: student age
 *         groupName:
 *           type: ['string','null']
 *           description: student groupName
 *       example:
 *         id: 1
 *         name: Ivan
 *         surname: Ivanov
 *         email: Ivan@mail
 *         age: 20
 *         groupName: math group
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentsWithMarks:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - imagePath
 *         - marks
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: student name
 *         surname:
 *           type: string
 *           description: student surname
 *         email:
 *           type: string
 *           description: student email
 *         age:
 *           type: number
 *           description: student age
 *         imagePath:
 *           type: ['string','null']
 *           description: student imagePath
 *         marks:
 *           type: ['array', 'null']
 *           description: student marks
 *           items:
 *              $ref: '#/components/schemas/Marks'
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Ivan
 *         surname: Ivanov
 *         email: Ivan@mail
 *         age: 20
 *         imagePath: path
 *         marks: []
 */

export interface IStudentGetResponse extends IStudent {
  createdAt: Date;
  updatedAt: Date;
}
