import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './student.interface';
import { Student } from '../entities/student.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentUpdateGroup:
 *       type: object
 *       properties:
 *        group:
 *          type: string
 *       example:
 *         group: 1
 */

export interface IStudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IStudent>;
}

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentUpdate:
 *       type: object
 *       properties:
 *        name:
 *          type: string
 *        surname:
 *          type: string
 *        age:
 *          type: number
 *        email:
 *          type: string
 *        imagePath:
 *          type: string
 *       example:
 *         email: example@email.com
 *         name: John
 *         surname: Doe
 *         age: 25
 *         imagePath: /path/to/image
 */

export interface IAddGroupToStudentRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<Student, 'group'>;
}
