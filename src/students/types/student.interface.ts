import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';

export interface IStudent {
  id: string;
  email: string;
  name: string;
  surname: string;
  age: number;
  group: Group;
  marks: Mark[];
  imagePath?: string;
}
