import 'dotenv/config';
import { AppDataSource } from '../src/configs/database/data-source';
import { Post } from '../src/posts/entities/post.entity';
import request from 'supertest';
import app, { appDataSourceInit } from '../src/application/app';
import { DataSource } from 'typeorm';

let dataSource: DataSource;

const postRepository = AppDataSource.getRepository(Post);

beforeAll(async () => {
  dataSource = await appDataSourceInit();
});

beforeEach(async () => {
  await postRepository.clear();
});

afterAll(async () => {
  await dataSource.destroy();
});

describe('/posts', () => {
  describe('POST /posts', () => {
    it('return 400 status if title is not passed', async () => {
      await request(app)
        .post('/api/v1/posts')
        .send({ content: 'Hello' })
        .expect(400);
    });

    it('return 400 status if content is not passed', async () => {
      await request(app)
        .post('/api/v1/posts')
        .send({ title: 'Hello' })
        .expect(400);
    });

    it('saves post', async () => {
      const title = 'New Post';
      const content = 'Content Post with info';

      await request(app)
        .post('/api/v1/posts')
        .send({ title, content })
        .expect(201);

      const postRepository = AppDataSource.getRepository(Post);

      const posts = await postRepository.find();

      expect(posts.length).toEqual(1);

      expect(posts[0]).toMatchObject({
        title,
        content,
      });
    });

    it('returns created posts', async () => {
      const response = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post', content: 'Content Post with info' })
        .expect(201);

      const posts = await postRepository.find();

      expect(posts.length).toEqual(1);

      const { id, title, content, createdAt, updateAt } = posts[0];

      expect(response.body).toEqual({
        id,
        title,
        content,
        createdAt: createdAt.toISOString(),
        updateAt: updateAt.toISOString(),
      });
    });
  });

  describe('GET /posts', () => {
    it('returns an empty list if no posts', async () => {
      const response = await request(app).get('/api/v1/posts').expect(200);

      expect(response.body).toEqual([]);
    });

    it('returns all saved posts', async () => {
      const { body: firstPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 1', content: 'Content Post 1 with info' })
        .expect(201);

      const { body: thirdPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 3', content: 'Some another info' })
        .expect(201);

      const { body: secondPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 2', content: 'Some another info' })
        .expect(201);

      const response = await request(app).get('/api/v1/posts').expect(200);

      expect(response.body).toEqual([secondPost, thirdPost, firstPost]);
    });

    it('returns post by id', async () => {
      const { body: secondPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 2', content: 'Some another info' })
        .expect(201);

      const { body: firstPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 1', content: 'Content Post with info' })
        .expect(201);

      const response = await request(app).get('/api/v1/posts').expect(200);

      const firstPostId = response.body[0].id;
      const secondPostId = response.body[1].id;

      const firstPostResponse = await request(app)
        .get(`/api/v1/posts/${firstPostId}`)
        .expect(200);
      const secondPostResponse = await request(app)
        .get(`/api/v1/posts/${secondPostId}`)
        .expect(200);

      expect(firstPostResponse.body).toEqual(firstPost);
      expect(secondPostResponse.body).toEqual(secondPost);
    });

    it('returns 404 if post id doesnt exist in database', async () => {
      await request(app).get('/api/v1/posts/1').expect(404);
    });
  });
  describe('PATCH /posts', () => {
    it('update post by id', async () => {
      const { body: secondPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 2', content: 'Some another info' })
        .expect(201);

      const { body: firstPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 1', content: 'Content Post with info' })
        .expect(201);

      const response = await request(app).get('/api/v1/posts').expect(200);

      const firstPostId = response.body[0].id;
      const secondPostId = response.body[1].id;

      await request(app)
        .patch(`/api/v1/posts/${firstPostId}`)
        .send({ title: 'changed New Post 1' })
        .expect(204);
      await request(app)
        .patch(`/api/v1/posts/${secondPostId}`)
        .send({ content: 'changed Content Post with info' })
        .expect(204);

      const firstPostResponse = await request(app)
        .get(`/api/v1/posts/${firstPostId}`)
        .expect(200);
      const secondPostResponse = await request(app)
        .get(`/api/v1/posts/${secondPostId}`)
        .expect(200);

      expect(firstPostResponse.body.title).toEqual(
        (firstPost.title = 'changed New Post 1'),
      );
      expect(secondPostResponse.body.content).toEqual(
        (secondPost.content = 'changed Content Post with info'),
      );
    });

    it('returns 404 trying update post which doesnt exist', async () => {
      await request(app)
        .patch(`/api/v1/posts/1`)
        .send({ title: 'changed New Post 1' })
        .expect(404);
    });

    it('returns 400 trying update post with incorrect body', async () => {
      await request(app)
        .patch(`/api/v1/posts/1`)
        .send({ ttl: 'changed New Post 1' })
        .expect(400);
    });
  });
  describe('DELETE /posts', () => {
    it('returns 404 trying delete post which doesnt exist', async () => {
      await request(app).delete(`/api/v1/posts/1`).expect(404);
    });

    it('delete post', async () => {
      const { body: secondPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 2', content: 'Some another info' })
        .expect(201);

      const response = await request(app).get('/api/v1/posts').expect(200);

      const secondPostId = response.body[0].id;

      const secondPostResponse = await request(app)
        .get(`/api/v1/posts/${secondPostId}`)
        .expect(200);

      expect(secondPostResponse.body).toEqual(secondPost);
      await request(app).delete(`/api/v1/posts/${secondPostId}`).expect(204);

      const responseAfterDelete = await request(app)
        .get('/api/v1/posts')
        .expect(200);

      expect(responseAfterDelete.body).toEqual([]);
    });
  });
});
